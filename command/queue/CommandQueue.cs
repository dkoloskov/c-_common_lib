﻿using System;
using System.Collections.Generic;

namespace common_lib.command.queue
{
    public class CommandQueue : IDisposable
    {
        protected Queue<CommandQueueItem> _queue;
        protected bool _started;
        protected ICommand _currentCommand;

        public CommandQueue()
        {
            _queue = new Queue<CommandQueueItem>();
            _started = false;
            _currentCommand = null;
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        public void Add(ICommand command, bool block = false, bool autoStart = false)
        {
            CommandQueueItem commandQueueItem = new CommandQueueItem(command, block);
            _queue.Enqueue(commandQueueItem);

            if (autoStart)
                this.Start();
        }

        public virtual void Start()
        {
            if (!_started)
                this.nextCommand();
        }

        public virtual void Clear()
        {
            ICommand command;
            while (_queue.Count > 0)
            {
                command = _queue.Dequeue().Command;
                command.Dispose();
            }

            this.cleanCommand();
            _started = false;
        }

        public void Dispose()
        {
            this.Clear();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected virtual void nextCommand()
        {
            this.cleanCommand();

            if (_queue.Count > 0)
            {
                Console.WriteLine("Command Queue: nextCommand");

                CommandQueueItem commandQueueItem = _queue.Dequeue();
                _currentCommand = commandQueueItem.Command;
                if (commandQueueItem.BlockAfterComplete)
                    _currentCommand.CommandComplete = this.cleanCommand;
                else
                    _currentCommand.CommandComplete = this.nextCommand;

                _started = true;
                _currentCommand.Execute();
            }
            else
            {
                _started = false;
            }
        }

        protected virtual void cleanCommand()
        {
            if (_currentCommand != null)
            {
                _currentCommand.Dispose();
                _currentCommand = null;
                _started = false;
            }
        }
    }
}