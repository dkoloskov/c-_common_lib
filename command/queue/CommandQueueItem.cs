﻿namespace common_lib.command.queue
{
    public class CommandQueueItem
    {
        public ICommand Command;
        public bool BlockAfterComplete;

        public CommandQueueItem(ICommand Command, bool BlockAfterComplete)
        {
            this.Command = Command;
            this.BlockAfterComplete = BlockAfterComplete;
        }
    }
}