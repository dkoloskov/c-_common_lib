﻿using System;
using System.Collections.Generic;


namespace common_lib.command
{
    public interface ICommand:IDisposable
    {
        void Execute();

        Action CommandComplete { set; }
    }
}
