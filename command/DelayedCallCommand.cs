﻿using common_lib.method.var;
using System;
using System.Collections.Generic;


namespace common_lib.command
{
    class DelayedCallCommand : CommandBase
    {
        //float _delay;
        IVarMethod _callback;

        public DelayedCallCommand(float delay, IVarMethod callback)
        {
            //_delay = delay;
            _callback = callback;
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        override public void Execute()
        {
            // TODO: add TIMER, which will call base.Execute() on complete
            throw new NotImplementedException();
        }

        override public void Dispose()
        {
            _callback = null;
            base.Dispose();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        override protected void execute()
        {
            _callback.Call();
        }
    }
}
