﻿using System;
using System.Collections.Generic;


namespace common_lib.command
{
    public abstract class CommandBase : ICommand
    {
        private static ulong s_id = 0; // s_ for static
        protected static ulong NEXT_ID { get { return CommandBase.s_id++; } }

        protected ulong id;
        protected Action _commandComplete;


        public CommandBase()
        {
            id = CommandBase.NEXT_ID;
        }

        // Template method
        public virtual void Execute()
        {
            this.execute();
            this.afterExecute();
        }

        public virtual void Dispose()
        {
            _commandComplete = null;
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        // Not abstract, because sometimes, it doesn't need to be used in subclass. example: DelayedCallCommand
        protected virtual void execute()
        {
            throw new NotImplementedException();
        }

        protected virtual void afterExecute()
        {
            if (_commandComplete != null)
                _commandComplete();
        }
        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public Action CommandComplete
        {
            set
            {
                _commandComplete = value;
            }
        }
    }
}
