﻿using common_lib.method.var;
using System;
using System.Collections.Generic;


namespace common_lib.command
{
    public class CallCommand : CommandBase
    {
        IVarMethod _callback;

        public CallCommand(IVarMethod callback)
        {
            _callback = callback;
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        override public void Dispose()
        {
            _callback = null;
            base.Dispose();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        override protected void execute()
        {
            _callback.Call();
        }
    }
}
