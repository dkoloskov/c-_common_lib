﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace common_lib.array
{
    // 1 dimensional  array - representation of 2 dimensional array
    public class Flatten2DArray<T> : IEnumerable<T>
    {
        protected T[] _items;

        public int Width;
        public int Height;

        public Flatten2DArray(int width = 0, int height = 0)
        {
            Width = width;
            Height = height;

            if (Width > 0 || Height > 0)
                _items = new T[Width * Height];
        }

        public Flatten2DArray(T[] items, int width, int height)
        {
            _items = items;
            Width = width;
            Height = height;

            validateConstructorArgs();
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        // implicit conversion
        public static implicit operator T[,](Flatten2DArray<T> value)
        {
            return ArrayUtils.to2D(value.Items, value.Width, value.Height);
        }

        // implicit conversion
        public static implicit operator Flatten2DArray<T>(T[,] values)
        {
            return new Flatten2DArray<T>(ArrayUtils.to1D(values), values.GetLength(0), values.GetLength(1));
        }

        public T this[int i, int j]
        {
            get { return _items[i * Height + j]; }
            set { _items[i * Height + j] = value; }
        }

        // Add - should be used only as Collection Initializer
        public void Add(params T[] args)
        {
            validateInitializerArgs(args);
            if (_items == null)
            {
                Width = 0;
                Height = args.Length;
            }
            Width++;

            T[] newItems = new T[Width * Height];
            if (_items != null)
                _items.CopyTo(newItems, 0);
            args.CopyTo(newItems, ((Width - 1) * Height));

            _items = newItems;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>) _items).GetEnumerator();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected void validateConstructorArgs()
        {
            int expectedLength = Width * Height;
            if (_items.Length != expectedLength)
                throw new ArgumentException("_items.Length(" + _items.Length + ") " +
                                            "!= expectedLength(" + expectedLength + ")");
        }

        protected void validateInitializerArgs(T[] args)
        {
            if (args == null || args.Length == 0)
                throw new ArgumentNullException();

            if (Height > 0 && args.Length != Height)
                throw new ArgumentException("args.Length(" + args.Length + ") != Height(" + Width + ")");
        }

        // TODO: investigate this construction, which is required to implement IEnumerable<>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public T[] Items
        {
            get { return _items; }
        }
    }
}