﻿using System;
using System.Linq;

namespace common_lib.array
{
    public class ArrayUtils
    {
        public static T[,] ResizeArray<T>(T[,] original,
            int newWidth, int newHeight,
            int originalShiftX = 0, int originalShiftY = 0,
            int newShiftX = 0, int newShiftY = 0
        )
        {
            var newArray = new T[newWidth, newHeight];

            int width = original.GetLength(0);
            int height = original.GetLength(1);

            int copyValuesByX = (newWidth < width) ? newWidth : width;
            int copyValuesByY = (newHeight < height) ? newHeight : height;
            for (int i = 0; i < copyValuesByX; i++)
            {
                Array.Copy(original, ((i + originalShiftX) * height + originalShiftY),
                    newArray, (i + newShiftX) * newHeight + newShiftY,
                    copyValuesByY);
            }
            return newArray;
        }

        // TODO: OPTIMIZE THIS
        public static T[] to1D<T>(T[,] original)
        {
            T[] array1D = original.Cast<T>().ToArray();
            return array1D;
        }

        // TODO: OPTIMIZE THIS
        public static T[,] to2D<T>(T[] original, int width, int height)
        {
            T[,] array2D = new T[width, height];

            int i = 0;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    array2D[x, y] = original[i];
                    i++;
                }
            }
            return array2D;
        }
    }
}