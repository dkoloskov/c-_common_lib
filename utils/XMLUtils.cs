﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;


namespace common_lib.utils
{
    public class XMLUtils
    {
        public static void displayTreeToConsole(XmlNode node, int depth=-1)
        {
            switch (node.NodeType)
            {
                case XmlNodeType.Element:
                    for (int i = 0; i < depth; i++)
                    {
                        Console.Write("---");
                    }
                    Console.Write(node.Name);

                    if (node.HasChildNodes && (node.FirstChild.NodeType != XmlNodeType.Text))
                    {
                        Console.WriteLine("");
                    }
                    break;

                case XmlNodeType.Text:
                    Console.WriteLine(" - {0}", node.Value);
                    break;
            }

            IEnumerator nodeChildren = node.GetEnumerator();
            XmlNode childNode;
            depth++;
            while (nodeChildren.MoveNext())
            {
                childNode = (XmlNode)nodeChildren.Current;
                displayTreeToConsole(childNode, depth);
            }
        }
    }
}
