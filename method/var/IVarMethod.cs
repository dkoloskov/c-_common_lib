﻿using System;
using System.Collections.Generic;


namespace common_lib.method.var
{
    public interface IVarMethod
    {
        void Call();
        void Call(params object[] args);
    }
}
