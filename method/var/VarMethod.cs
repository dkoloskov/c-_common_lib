﻿using System;
using System.Collections.Generic;


namespace common_lib.method.var
{
    /// <summary>
    /// Class container for method, and arguments passed to method.
    /// Note: arguments can be passed on method call.
    /// </summary>
    public class VarMethod : AbstractVarMethod
    {
        public VarMethod(Action action)
            : base(action.Target, action.Method, new object[] { })
        {
        }
    }

    // Constructors for object and object[] separated, and record "params object[] args" can't be used,
    // because misunderstanding when array passed to method, array(1 argument) was converted to arguments.
    public class VarMethod<T1>:AbstractVarMethod
    {
        // if 1 argument required to be passed
        public VarMethod(Action<T1> action, T1 arg) : 
            base(action.Target, action.Method, new object[] { arg })
        {
        }

        // if 1 array argument required to be passed
        public VarMethod(Action<T1> action, object[] args) :
            base(action.Target, action.Method, new object[] { args })
        {
        }
    }

    public class VarMethod<T1, T2> : AbstractVarMethod
    {
        public VarMethod(Action<T1, T2> action, params object[] args) :
            base(action.Target, action.Method, args)
        {
        }
    }

    public class VarMethod<T1, T2, T3> : AbstractVarMethod
    {
        public VarMethod(Action<T1, T2, T3> action, params object[] args) :
            base(action.Target, action.Method, args)
        {
        }
    }

    public class VarMethod<T1, T2, T3, T4> : AbstractVarMethod
    {
        public VarMethod(Action<T1, T2, T3, T4> action, params object[] args) :
            base(action.Target, action.Method, args)
        {
        }
    }

    // TODO: Need to find solution. Doesn't work for some projects
    // Don't like solution to write many classes, but can't find better way right now
    /*public class VarMethod<T1, T2, T3, T4, T5> : AbstractVarMethod
    {
        public VarMethod(Action<T1, T2, T3, T4, T5> action, params object[] args) :
            base(action.Target, action.Method, args)
        {
        }
    }*/
}
