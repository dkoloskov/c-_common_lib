﻿using System;
using System.Collections.Generic;
using System.Reflection;


namespace common_lib.method.var
{
    public abstract class AbstractVarMethod:IVarMethod
    {
        private object _context; // instance of class, which method will be called
        private MethodInfo _methodInfo;
        private List<object> _argsList;

        private int _numberOfPassedArguments; // this value only for throw error, if will be required


        public AbstractVarMethod(object context, MethodInfo methodInfo, object[] args)
        {
            _context = context;
            _methodInfo = methodInfo;
            setArguments(args);
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        public void Call()
        {
            _methodInfo.Invoke(_context, _argsList.ToArray());
        }

        public void Call(params object[] args)
        {
            setArguments(args);
            this.Call();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        private void setArguments(object[] args)
        {
            _argsList = new List<object>();

            // If arguments haven't been passed to constructor, no error will be thrown, they can be passed to Call method.
            // But, arguments also can be missed on Call, this would provoke default ArgumentException.
            if (args.Length > 0)
            {
                _argsList = new List<object>(args);
                _numberOfPassedArguments = _argsList.Count;

                checkAndFixNumberOfArguments();
            }
        }

        // If arguments length == method parameters quantity - OK(return)
        // If arguments length < method parameters quantity
        //      if method has default arguments - Type.Missing will be used, then OK(return)
        //      if method don't has default arguments - Error will be thrown
        private void checkAndFixNumberOfArguments()
        {
            ParameterInfo[] parametersInfo = _methodInfo.GetParameters();
            if(_argsList.Count == parametersInfo.Length)
            {
                return;
            }
            else if(_argsList.Count > parametersInfo.Length)
            {
                // If more arguments, than method parameters. Too many arguments passed to method
                throw new ArgumentException(getWrongNumberOfArgumentsExceptionText());
            }

            // TODO: Need to find solution. Doesn't work for some projects
            /*ParameterInfo parameterInfo;
            for(int i= _argsList.Count; i<parametersInfo.Length; i++)
            {
                parameterInfo = parametersInfo[i];
                if(parameterInfo.HasDefaultValue)
                {
                    _argsList.Add(Type.Missing);
                }
                else
                {
                    // If not enough arguments passed to method
                    throw new ArgumentException(getWrongNumberOfArgumentsExceptionText());
                }
            }*/
            throw new ArgumentException(getWrongNumberOfArgumentsExceptionText());
        }

        private string getWrongNumberOfArgumentsExceptionText()
        {
            string exceptionText = "";
            if (_context != null)
            {
                exceptionText += _context.ToString() + ":";
            }
            exceptionText += _methodInfo.Name + "() ";
            exceptionText += "Wrong number of arguments " + _numberOfPassedArguments +
                             " instead of " + _methodInfo.GetParameters().Length;

            return exceptionText;
        }
    }
}
