﻿namespace common_lib.graph.enums
{
    public enum PassTypesEnum : int
    {
        UP = MessageDirectionsEnum.UP, // -1
        NOWHERE = 0,
        DOWN = MessageDirectionsEnum.DOWN, // 1
        ANY = 2
    };
}
