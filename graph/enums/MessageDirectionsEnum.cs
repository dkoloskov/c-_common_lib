﻿namespace common_lib.graph.enums
{
    public enum MessageDirectionsEnum : sbyte
    {
        UP = -1,
        DOWN = 1
    };
}
