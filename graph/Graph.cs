﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using common_lib.graph.enums;
using common_lib.graph.node;

namespace common_lib.graph
{
    public sealed class Graph:IDisposable
    {
        private const string HANDLER_SEPARATOR = "&";
		private static Graph _instance;
		private Dictionary<string, INode> _map;
		private string _indexes;
		private string _handlerIndexes;

        /// <summary>
        /// "_d" & "_s" in method names - to distinguish static and dynamic methods with same name.
        /// </summary>
        private Graph() { }

        public static void AddNode(INode node, bool updateIndexesAfter = true)
		{
			_instance.addNode(node, updateIndexesAfter);
		}

        public static void Dispose_s()
		{
			if (_instance != null)
			{
				_instance.Dispose();
				_instance = null;
			}
		}

        public static void FireMessage(string path, MessageDirectionsEnum direction, string type)
        {
            FireMessage<object>(path, direction, type);
        }

        public static void FireMessage(string path, MessageDirectionsEnum direction, string type, object data = null)
        {
            FireMessage<object>(path, direction, type, data);
        }

        public static void FireMessage<T>(string path, MessageDirectionsEnum direction, string type, T data = default(T))
		{
            _instance.fireMessage(path, direction, type, data);
		}

        public static List<INode> GetChildren(string path)
		{
			return _instance.getChildren(path);
		}

        public static List<INode> GetChildrenWithHandler(string path, string handlerType)
		{
			return _instance.getChildrenWithHandler(path, handlerType);
		}

        public static INode GetNode(string path)
		{
			return _instance.getNode(path);
		}

        public static List<INode> GetParents(string path, string until = null)
		{
			return _instance.getParents(path, until);
		}

        public static void Initialize()
		{
			_instance = new Graph();
            _instance.initialize();
		}

		public static void RemoveNode(string path, bool updateIndexesAfter = true)
		{
			_instance.removeNode(path, updateIndexesAfter);
		}

		public static void RemoveNodes(string path, bool updateIndexesAfter = true)
		{
			_instance.removeNodes(path, updateIndexesAfter);
		}

		public static void UpdateIndexes()
		{
			_instance.updateIndexes();
		}

        private void initialize()
		{
            _map = new Dictionary<string, INode>();
        }

        private void addNode(INode node, bool updateIndexesAfter = true)
		{
            if (_map.ContainsKey(node.Path))
			{
                throw new InvalidOperationException("Node with such path: '" + node.Path + "' already exists.");
            }

            _map[node.Path] = node;
			if (updateIndexesAfter)
			{
                updateIndexes();
            }
		}

        private void fireMessage<T>(string path, MessageDirectionsEnum direction, string type, T data = default(T))
		{
			INode node;
			List<INode> nodes;
            List<INode> nodesToBlock;
			List<INode> parents;
            INode parent;
			Dictionary<string, bool> blockedPath;
			int i;
			int j;
			int k;
			object handler;

			if (direction == MessageDirectionsEnum.UP)
			{
				parents = getParents(path);
				for (i = 0; i<parents.Count; i++)
				{
					parent = parents[i];
					if (parent.PassType == PassTypesEnum.DOWN || parent.PassType == PassTypesEnum.NOWHERE)
					{
						if (parent.Handlers.ContainsKey(type))
						{
							handler = parent.Handlers[type];
                            callNodeMessageHandler(handler, data, direction);
                            if (_map == null)
							{
								return;
							}
						}
						else
						{
                            callNodeOnBlock<T>(parent, direction, type, data);
							if (_map == null)
							{
								return;
							}
						}
						break;
					}
					else
					{
						if (parent.Handlers.ContainsKey(type))
						{
							handler = parent.Handlers[type];
                            callNodeMessageHandler(handler, data, direction);
                            if (_map == null)
							{
								return;
							}
						}
					}

					if (_map == null)
					{
						return;
					}
				}
			}
			else
			{
				nodes = getChildrenWithHandler(path, type);
                blockedPath = new Dictionary<string, bool>();
                for (i = 0; i<nodes.Count; i++)
				{
					node = nodes[i];
					if (blockedPath.ContainsKey(node.Path))
					{
						continue;
					}
					if (node.PassType == PassTypesEnum.UP || node.PassType == PassTypesEnum.NOWHERE)
					{
                        // TODO: +++ INVESTIGATE THIS SHIT +++
                        if (node.Handlers.ContainsKey(type))
						{
							handler = node.Handlers[type];
                            callNodeMessageHandler(handler, data, direction);
                            if (_map == null)
							{
								return;
							}
						}
						else
						{
                            callNodeOnBlock<T>(node, direction, type, data);
                            if (_map == null)
							{
								return;
							}
						}
						nodesToBlock = getChildrenWithHandler(node.Path, type);
						for (j = 0; j<nodesToBlock.Count; j++)
						{
							blockedPath[nodesToBlock[j].Path] = true;
						}
                        // TODO: --- INVESTIGATE THIS SHIT ---
                    }
                    else
					{
						parents = getParents(node.Path, path);
						for (j = parents.Count - 1; j >= 0; j--)
						{
							parent = parents[j];
							if (parent.PassType == PassTypesEnum.UP || parent.PassType == PassTypesEnum.NOWHERE)
							{
								if (parent.Handlers.ContainsKey(type))
								{
									handler = parent.Handlers[type];
                                    callNodeMessageHandler(handler, data, direction);
                                    if (_map == null)
									{
										return;
									}
								}
								else
								{
                                    callNodeOnBlock<T>(parent, direction, type, data);
                                    if (_map == null)
									{
										return;
									}
								}
								nodesToBlock = getChildrenWithHandler(parent.Path, type);
								for (k = 0; k<nodesToBlock.Count; k++)
								{
									blockedPath[nodesToBlock[k].Path] = true;
								}
								break;
							}
							if (_map == null)
							{
								return;
							}
						}
						if (blockedPath.ContainsKey(node.Path))
						{
							continue;
						}
						if (node.Handlers.ContainsKey(type))
						{
							handler = node.Handlers[type];
                            callNodeMessageHandler(handler, data, direction);
                            if (_map == null)
							{
								return;
							}
						}
					}

					if (_map == null)
					{
						return;
					}
				}
			}
		}

        // NEW METHOD. This method isn't moved from as3. One step (of many) to imporove Graph code.
        private void callNodeOnBlock<T>(INode node, MessageDirectionsEnum direction, string type, T data)
        {
            node.OnBlock<T>(direction, type, data);
        }

        // NEW METHOD. This method isn't moved from as3. One step (of many) to imporove Graph code.
        private void callNodeMessageHandler<T>(object handler, T data, MessageDirectionsEnum direction)
        {
            if (handler is NodeMessageHandler) (handler as NodeMessageHandler)();
            if (handler is NodeMessageHandlerWithDirection) (handler as NodeMessageHandlerWithDirection)(direction);
            if (handler is NodeMessageHandlerWithObject) (handler as NodeMessageHandlerWithObject)(data);
            if (handler is NodeMessageHandlerWithObjectAndDirection) (handler as NodeMessageHandlerWithObjectAndDirection)(data, direction);
            if (handler is NodeMessageHandlerWithData<T>) (handler as NodeMessageHandlerWithData<T>)(data);
            if (handler is NodeMessageHandlerWithDataAndDirection<T>) (handler as NodeMessageHandlerWithDataAndDirection<T>)(data, direction);
        }

        private List<INode> getChildren(string path)
		{
            List<INode> result = new List<INode>();
            Regex regex = new Regex(path + "(.*)");

            MatchCollection matches = regex.Matches(_indexes);
            foreach (Match match in matches)
            {
                if(match.Value != path)
                {
                    result.Add(_map[match.Value]);
                }
            }

            return result;
		}

        private List<INode> getChildrenWithHandler(string path, string handlerType)
		{
			List<INode> result = new List<INode>();
            Regex regex = new Regex("^" + handlerType + HANDLER_SEPARATOR + path + "(.*)", RegexOptions.Multiline);

            int sliceLength = (handlerType + HANDLER_SEPARATOR).Length;
            MatchCollection matches = regex.Matches(_handlerIndexes);
            string realKey;
            foreach (Match match in matches)
            {
                realKey = match.Value;
                realKey = realKey.Substring(sliceLength);
                result.Add(_map[realKey]);
            }

            return result;
		}

        private INode getNode(string path)
		{
			return _map[path];
		}

        private List<INode> getParents(string path, string until = null)
		{
			List<INode> result = new List<INode>();
			string workingPath;

			if (path == until)
			{
				return result;
			}

            workingPath = path;
			while (workingPath.Length > 0)
			{
                workingPath = workingPath.Substring(0, workingPath.Length - 1);
                if (workingPath == until)
				{
					return result;
				}
				if (_map.ContainsKey(workingPath))
                {
                    result.Add(_map[workingPath]);
                }
            }
			return result;
		}

        private void removeNode(string path, bool updateIndexesAfter)
		{
			if(_map.ContainsKey(path))
			{
                ((IDisposable) _map[path]).Dispose();
                _map.Remove(path);
            }
			if (updateIndexesAfter)
			{
                updateIndexes();
            }
		}

		private void removeNodes(string path, bool updateIndexesAfter)
		{
            Regex regex = new Regex(path + "(.*)");
            MatchCollection matches = regex.Matches(_indexes);
            foreach(Match match in matches)
            {
                ((IDisposable)_map[match.Value]).Dispose();
                _map.Remove(match.Value); // match.Value - path
            }

            if (updateIndexesAfter)
			{
                updateIndexes();
			}
		}

        private void updateIndexes()
		{
            List<string> sortedNodeKeys = new List<string>();
            List<string> sortedHandlersKeys = new List<string>();

            foreach (INode node in _map.Values)
            {
                sortedNodeKeys.Add(node.Path);
                foreach (string handlerType in node.Handlers.Keys)
                {
                    sortedHandlersKeys.Add(handlerType + HANDLER_SEPARATOR + node.Path);
                }
            }
            sortedNodeKeys.Sort();
            sortedHandlersKeys.Sort();

            _indexes = "";
            int index;
            for (index = 0; index < sortedNodeKeys.Count; index++)
			{
				_indexes += "\n" + sortedNodeKeys[index];
			}

            _handlerIndexes = "";
			for (index = 0; index < sortedHandlersKeys.Count; index++)
			{
				_handlerIndexes += "\n" + sortedHandlersKeys[index];
			}
        }

        public void Dispose()
		{
			if(_map != null)
            {
                foreach (IDisposable node in _map.Values)
                {
                    node.Dispose();
                }
                _map.Clear();
                _map = null;
            }

            _indexes = null;
			_handlerIndexes = null;
		}
    }
}
