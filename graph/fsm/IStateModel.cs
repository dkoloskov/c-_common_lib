﻿namespace common_lib.graph.fsm
{
    public interface IStateModel
    {
        string State { get; set; }
    }
}
