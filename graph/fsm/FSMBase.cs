﻿using System;
using common_lib.graph.enums;
using common_lib.graph.node;

namespace common_lib.graph.fsm
{
    public abstract class FSMBase:Node
    {
        public FSMBase(String path, PassTypesEnum passType = PassTypesEnum.UP, bool setup = true)
            :base(path, passType, setup)
        {

        }

        public override void OnBlock<T>(MessageDirectionsEnum direction, string type, T data = default(T))
        {
            Graph.FireMessage<T>(Path + stateModel.State, direction, type, data);
        }

        protected abstract IStateModel stateModel { get; }
    }
}
