﻿using System;
using common_lib.command;
using common_lib.command.queue;
using common_lib.graph.enums;
using common_lib.graph.node;
using common_lib.method.var;

namespace common_lib.graph.fsm
{
    public abstract class FSMStateBase:Node
    {
        protected CommandQueue _queue;

        public FSMStateBase (string path, PassTypesEnum passType = PassTypesEnum.ANY)
            :base(path, passType)
        {
            this.initialize();
        }

        protected virtual void initialize()
	    {
		    _queue = new CommandQueue();
        }

        protected virtual void activate()
        {
            Console.WriteLine(Name + " state activated");
        }

        override protected void setupHandlers()
	    {
		    base.setupHandlers();
		    AddHandler(CHANGE_STATE_MESSAGE, changeStateHandler);
        }

        protected virtual void changeState(string newState)
        {
            deactivate();
            stateModel.State = newState;
            FireMessage(CHANGE_STATE_MESSAGE);
        }

        protected void changeStateHandler()
	    {
            this.activate();
        }

        // +++++++++++++++++++++++++++++++++++++ addCallToQueue +++++++++++++++++++++++++++++++++++++
        protected void addCallToQueue(Action callback)
        {
            _queue.Add(new CallCommand(new VarMethod(callback)));
        }

        protected void addCallToQueue<T1>(Action<T1> callback, T1 args)
        {
            _queue.Add(new CallCommand(new VarMethod<T1>(callback, args)));
        }

        protected void addCallToQueue<T1, T2>(Action<T1,T2> callback, T1 arg1, T2 arg2)
        {
            _queue.Add(new CallCommand(new VarMethod<T1,T2>(callback, arg1, arg2)));
        }

        protected void addCallToQueue<T1, T2, T3>(Action<T1, T2, T3> callback, T1 arg1, T2 arg2, T3 arg3)
        {
            _queue.Add(new CallCommand(new VarMethod<T1, T2, T3>(callback, arg1, arg2, arg3)));
        }
        // ------------------------------------- addCallToQueue -------------------------------------

        // +++++++++++++++++++++++++++++++++++++ addCallToQueueWithLock +++++++++++++++++++++++++++++++++++++
        protected void addCallToQueueWithLock(Action callback)
        {
            _queue.Add(new CallCommand(new VarMethod(callback)), true);
        }

        protected void addCallToQueueWithLock<T1>(Action<T1> callback, T1 args)
        {
            _queue.Add(new CallCommand(new VarMethod<T1>(callback, args)), true);
        }

        protected void addCallToQueueWithLock<T1, T2>(Action<T1, T2> callback, T1 arg1, T2 arg2)
        {
            _queue.Add(new CallCommand(new VarMethod<T1, T2>(callback, arg1, arg2)), true);
        }

        protected void addCallToQueueWithLock<T1, T2, T3>(Action<T1, T2, T3> callback, T1 arg1, T2 arg2, T3 arg3)
        {
            _queue.Add(new CallCommand(new VarMethod<T1, T2, T3>(callback, arg1, arg2, arg3)), true);
        }
        // ------------------------------------- addCallToQueueWithLock -------------------------------------

        /*protected addDelayedCallToQueue(delay:number, callback:Function, ...args:any[])
        {
            this._queue.add(new DelayedCallCommand(delay, callback, args));
        }

        protected addDelayedCallToQueueWithLock(delay:number, callback:Function, ...args:any[])
        {
            this._queue.add(new DelayedCallCommand(delay, callback, args), true);
        }*/

        protected virtual void deactivate()
        {
            _queue.Clear();
            Console.WriteLine(Name + " state DEACTIVATED");
        }

        override public void Dispose()
        {
            base.Dispose();
            if (_queue != null)
            {
                _queue.Clear();
                _queue = null;
            }
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------        
        protected abstract IStateModel stateModel { get; }

        protected abstract string CHANGE_STATE_MESSAGE { get; }
    }
}
