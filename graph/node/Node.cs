﻿using System.Collections.Generic;
using common_lib.graph.enums;

namespace common_lib.graph.node
{
    public abstract class Node : INode
    {
        public const string SEPARATOR = "#";

        protected string _pathWithoutName;
        protected string _path;
        protected PassTypesEnum _passType;
        protected Dictionary<string, object> _handlers;

        public Node(string path, PassTypesEnum passType = PassTypesEnum.ANY, bool setup = true)
        {
            _pathWithoutName = path;
            _passType = passType;

            if (setup)
            {
                setupNode();
            }
        }

        protected virtual void setupNode()
        {
            setupChildren();
            setupHandlers();
        }

        protected virtual void setupChildren()
        {
        }

        protected virtual void setupHandlers()
        {
            _handlers = new Dictionary<string, object>();
        }

        // ++++++++++++++++++++ AddHandler ++++++++++++++++++++
        public void AddHandler(string type, NodeMessageHandler handler, bool updateIndexesAfter = false)
        {
            addHandler(type, handler, updateIndexesAfter);
        }

        public void AddHandler(string type, NodeMessageHandlerWithDirection handler, bool updateIndexesAfter = false)
        {
            addHandler(type, handler, updateIndexesAfter);
        }

        public void AddHandler(string type, NodeMessageHandlerWithObject handler, bool updateIndexesAfter = false)
        {
            addHandler(type, handler, updateIndexesAfter);
        }

        public void AddHandler(string type, NodeMessageHandlerWithObjectAndDirection handler,
            bool updateIndexesAfter = false)
        {
            addHandler(type, handler, updateIndexesAfter);
        }

        public void AddHandler<T>(string type, NodeMessageHandlerWithData<T> handler, bool updateIndexesAfter = false)
        {
            addHandler(type, handler, updateIndexesAfter);
        }

        public void AddHandler<T>(string type, NodeMessageHandlerWithDataAndDirection<T> handler,
            bool updateIndexesAfter = false)
        {
            addHandler(type, handler, updateIndexesAfter);
        }
        // -------------------- AddHandler --------------------

        public void AddNode(INode node, bool updateIndexesAfter = false)
        {
            Graph.AddNode(node, updateIndexesAfter);
        }

        // ++++++++++++++++++++ FireMessage ++++++++++++++++++++
        public void FireMessage(string type)
        {
            FireMessage<int>(type); // int - used because it fastest for performance data type
        }

        public void FireMessage(string type, object data = null,
            MessageDirectionsEnum direction = MessageDirectionsEnum.UP)
        {
            FireMessage<object>(type, data, direction);
        }

        public void FireMessage<T>(string type, T data = default(T),
            MessageDirectionsEnum direction = MessageDirectionsEnum.UP)
        {
            Graph.FireMessage(Path, direction, type, data);
        }
        // -------------------- FireMessage --------------------

        public virtual void OnBlock<T>(MessageDirectionsEnum direction, string type, T data = default(T))
        {
        }

        public void RemoveHandler(string type, bool updateIndexesAfter = true)
        {
            _handlers.Remove(type);
            if (updateIndexesAfter)
            {
                Graph.UpdateIndexes();
            }
        }

        public virtual void Dispose()
        {
            _path = null;
            _handlers = null;
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        private void addHandler(string type, object handler, bool updateIndexesAfter = false)
        {
            _handlers[type] = handler;
            if (updateIndexesAfter)
            {
                Graph.UpdateIndexes();
            }
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public Dictionary<string, object> Handlers
        {
            get { return _handlers; }
        }

        public abstract string Name { get; }

        public PassTypesEnum PassType
        {
            get { return _passType; }
        }

        public string Path
        {
            get
            {
                if (_path == null)
                {
                    _path = _pathWithoutName + Name + SEPARATOR;
                }

                return _path;
            }
        }
    }
}