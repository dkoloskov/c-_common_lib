﻿using System;
using System.Collections.Generic;
using common_lib.graph.enums;

namespace common_lib.graph.node
{
    public interface INode:IDisposable
    {
        void AddHandler(string type, NodeMessageHandler handler, bool updateIndexesAfter = false);
        void AddHandler(string type, NodeMessageHandlerWithDirection handler, bool updateIndexesAfter = false);
        void AddHandler(string type, NodeMessageHandlerWithObject handler, bool updateIndexesAfter = false);
        void AddHandler(string type, NodeMessageHandlerWithObjectAndDirection handler, bool updateIndexesAfter = false);
        void AddHandler<T>(string type, NodeMessageHandlerWithData<T> handler, bool updateIndexesAfter = false);
        void AddHandler<T>(string type, NodeMessageHandlerWithDataAndDirection<T> handler, bool updateIndexesAfter = false);

        void AddNode(INode node, bool updateIndexesAfter = false);

        void FireMessage(string type);
        void FireMessage<T>(string type, T data = default(T), MessageDirectionsEnum direction = MessageDirectionsEnum.UP);

        void OnBlock<T>(MessageDirectionsEnum direction, string type, T data = default(T));

        void RemoveHandler(string type, bool updateIndexesAfter = true);

        Dictionary<string, object> Handlers { get; } // TO DO: BE CAREFUL WITH IMPLEMENTATION OF THIS

        string Name { get; }

        PassTypesEnum PassType { get; }

		string Path { get; }
    }
}
