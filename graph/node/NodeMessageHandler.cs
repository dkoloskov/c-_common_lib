﻿using common_lib.graph.enums;

namespace common_lib.graph.node
{
    // Action can be used instead, but with custom delegates everything more clear
    // + No need to write arguments types, except for T.
    public delegate void NodeMessageHandler();
    public delegate void NodeMessageHandlerWithDirection(MessageDirectionsEnum direction);
    public delegate void NodeMessageHandlerWithObject(object data);
    public delegate void NodeMessageHandlerWithObjectAndDirection(object data, MessageDirectionsEnum direction);
    public delegate void NodeMessageHandlerWithData<T>(T data); // This type can be easily changed with Action
    public delegate void NodeMessageHandlerWithDataAndDirection<T>(T data, MessageDirectionsEnum direction); // This type can be easily changed with Action
}
